#!/usr/bin/env python3
import os
import time
from typing import Any
from fastapi import FastAPI
from fastapi.openapi.docs import get_swagger_ui_html
from pydantic_settings import BaseSettings
from starlette.responses import RedirectResponse
from functools import lru_cache, wraps
from httpx import Client
from pathlib import Path


class Settings(BaseSettings):
    tool_data_dir: Path = Path(os.environ.get("TOOL_DATA_DIR", ".")).absolute()
    toolforge_openapi_url: str = "https://127.0.0.1:30003/openapi.json"


app = FastAPI(docs_url=None, redoc_url=None, title="Toolforge API")
settings = Settings()


def timed_cache(max_age_min: int):
    def _decorator(func):
        @lru_cache(maxsize=1)
        def _with_time(*args, __time, **kwargs):
            return func(*args, **kwargs)

        @wraps(func)
        def _wrapper(*args, **kwargs):
            return _with_time(
                *args, **kwargs, __time=int(time.time() / (max_age_min * 60))
            )

        return _wrapper

    return _decorator


@app.get("/")
def root():
    return RedirectResponse("/docs")


@app.get("/docs", include_in_schema=False)
async def custom_swagger_ui_html():
    return get_swagger_ui_html(
        openapi_url=app.openapi_url,
        title=app.title + " - API UI",
        oauth2_redirect_url=app.swagger_ui_oauth2_redirect_url,
        swagger_js_url="https://tools-static.wmflabs.org/cdnjs/ajax/libs/swagger-ui/5.17.14/swagger-ui-bundle.min.js",
        swagger_css_url="https://tools-static.wmflabs.org/cdnjs/ajax/libs/swagger-ui/5.17.14/swagger-ui.css",
        swagger_favicon_url="https://static.toolforge.org/favicon.ico",
    )


@app.get("/healthz")
def healthz():
    return {"status": "ok"}


@timed_cache(max_age_min=15)
def get_toolforge_openapi() -> dict[str, Any]:
    with Client(
        verify=False,
        cert=(
            str(settings.tool_data_dir / ".toolskube/client.crt"),
            str(settings.tool_data_dir / ".toolskube/client.key"),
        ),
    ) as client:
        response = client.get(settings.toolforge_openapi_url)

    response.raise_for_status()

    return response.json()


app.openapi = get_toolforge_openapi  # type: ignore[method-assign]
